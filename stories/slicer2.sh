#!/bin/bash
TXT=$1
SLICEWIDTH=$2 

cat $TXT | fold -w $SLICEWIDTH | sed 's/^/"/g' | sed 's/$/",/g'